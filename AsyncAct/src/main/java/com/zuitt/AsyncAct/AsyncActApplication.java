package com.zuitt.AsyncAct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class AsyncActApplication {

	private Long postid;
	private Post post;

	public static void main(String[] args) {
        SpringApplication.run(AsyncActApplication.class, args);
	}

	//Session 10 activity

	//	Creating a new users
	//	localhost:8080/users
	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public String createUsers(){
        return "New user created.";
	}

	//	Retrieve all users
	//	localhost:8080/users
	@RequestMapping(value="/users", method = RequestMethod.GET)
	public String getUsers(){
        return "All users retrieved.";
	}

    //get specific user
    // localhost:8080/users/1234
    @RequestMapping(value="/users/{usersid}", method = RequestMethod.GET)
    public String getUsers(@PathVariable Long usersid){
        return "Viewing details of users " + usersid;
    }

	//	Deleting a users
	//	localhost:8080/posts/1234
	@RequestMapping(value = "/users/{users}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUser(@PathVariable Long usersid, @RequestHeader("Authorization") String users){
        if(users != null && !users.isEmpty()){
            return ResponseEntity.ok().body("The User" + usersid + "has been deleted");
        }else{
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized access");
        }
    }

	//	Updating a post
	//localhost:8080/posts/1234
	@RequestMapping(value = "/users/{users}", method = RequestMethod.PUT)
	@ResponseBody
	public Users updateName(@PathVariable Long usersid, @RequestBody Users users){
		return users;
	}

	//	//	Retrieve all posts
////	localhost:8080/posts
//	@RequestMapping(value="/posts", method = RequestMethod.GET)
//	//@GetMapping("/posts")
//	public String getPosts(){
//		return "All posts retrieved.";
//	}
//
//	//	Creating a new post
////	localhost:8080/posts
//	@RequestMapping(value = "/posts", method = RequestMethod.POST)
//	//@PostMapping("/posts")
//	public String createPost(){
//		return "New post created.";
//	}
//
//	//	Retrieving a single post
////	localhost:8080/posts/1234
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.GET)
//	//@GetMapping("/posts/{postid}")
//	public String getPost(@PathVariable Long postid){
//		return "Viewing details of post " + postid;
//	}
//
//	//	Deleting a post
////	localhost:8080/posts/1234
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
//	//@DeleteMapping("/posts/{postid}")
//	public String deletePost(@PathVariable Long postid){
//		return "The post " + postid + " has been deleted.";
//	}
//
//	//	Updating a post
////	localhost:8080/posts/1234
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
////	Automatically converts the format to JSON.
//	//@PutMapping("/posts/{postid}")
//	@ResponseBody
//	public Long updatePost(@PathVariable Long postid, @RequestBody Post post){
//		return postid;
//	}
//
//	//	Retrieving a posts for a particular user.
////	localhost:8080/myPosts
//	@RequestMapping(value = "/myPosts", method = RequestMethod.GET)
//	//@GetMapping("/myPosts")
//	public String getMyPosts(@RequestHeader(value = "Authorization") String user ) {
//			return "Posts for " + user + " have been retrieved";
//		}
//	}


}





